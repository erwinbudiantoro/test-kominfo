// MGPanelAjax V 1.0
// Dibuat Agustus 2013
// Author : Erwin Budiantoro
// Keterangan : Ajax ini hanya kompatibel pada halaman Admin MGPanel

// variabel base_url ini wajib diubah setiap kali ada perubahan nama domain maupun perpindahan dari localhost ke hosting
	
	
	function loading(pesan)
	{
	document.getElementById("proses").innerHTML='<img src="'+ base_url +'"assets/images/loading.gif"  /> &nbsp; '+pesan;
	}
	
	function ajaxpost(action, controller, funct, id)
	{
	document.getElementById("loading").innerHTML='<img src="'+ base_url +'assets/images/loading.gif"  /> Sedang memproses ..';
    $.ajax({
        url: base_url + action, 
        data: $(document.ajaxform.elements).serialize(), 
        success: function(responseText)
				{
                if(responseText == 'sukses')
					{
					window.alert('Proses Berhasil');
					buka(controller, funct, id);
					}
				else
					{
					window.alert('Proses Gagal');
					}
				},		
        type: "post", 
        dataType: "html"
    }); 
	}	

	function pencarian(controller,kata) 
	{
	if(kata.length == 0) 
		{
			document.getElementById("dataPencarian").innerHTML='';
		}		
		else if (kata.length > 3)
		{
		document.getElementById("dataPencarian").innerHTML='<div class="alert" align="center" style="margin:10px 200px; "><img src="'+ base_url +'assets/images/loading.gif"  /> Sedang memproses ..</div>';
			$.post(base_url +"mgpanel/"+controller+"/pencarian", {cari: ""+kata+""}, function(data){
				if(data.length > 0) {
					$('#dataPencarian').html(data);
				}
			});
		}	
	}	
	
	function chained(controller,id,target) 
	{
		document.getElementById("kedua").innerHTML='<img src="'+ base_url +'assets/images/loading.gif"  /> Sedang memproses ..';
			$.post(base_url +"mgpanel/"+controller+"/"+target, {cari: ""+id+""}, function(data){
			$('#kedua').html(data);
			});
	} 
	
	function harusangka(jumlah)
	{
		var karakter = (jumlah.which) ? jumlah.which : event.keyCode
		if (karakter > 31 && (karakter < 48 || karakter > 57))
			{
			return false;
			}
		else
			{
			return true;
			}
	}

	var mywind; 
	function popup_window(link)
	{
		mywind=window.open(base_url + 'mgpanel/' + link,"_blank","toolbar=0,location=0,scrollbars=1,addresbar=0,menubar=0,copyhistory=0,directories=0,status=0,resizable=yes,width=1100,height=700"); mywind.moveTo(200,300);
		}
		
	function buka(controller, funct, id)
	{
		window.scrollTo(0,5);
		var pageajax;
		if (window.XMLHttpRequest)
		  { // code for IE7+, Firefox, Chrome, Opera, Safari
		  pageajax=new XMLHttpRequest();
		  }
		else
		  { // code for IE6, IE5
		  pageajax=new ActiveXObject("Microsoft.XMLHTTP");
		  }
		  
		pageajax.onreadystatechange=function()
		  {
			if (pageajax.readyState==4 && pageajax.status==200)
				{
				document.getElementById("page").innerHTML=pageajax.responseText;
				}
			else
				{
				document.getElementById("page").innerHTML='<div style="margin:100px 0; font-size:11px; color:#999699;" align="center"><img src="'+ base_url +'assets/images/loading.gif"  /><br />sedang memuat halaman ...</div>';
				}	
		  }

		pageajax.open("GET",base_url +"mgpanel/"+ controller + "/" + funct +"?id="+id, true);
		pageajax.send();
		}
		
	function hapus(controller, funct, id, back)
	{
	  if (confirm("Anda yakin ?")) 
	  {
	  	var ajaxdel;
		if (window.XMLHttpRequest)
		  { // code for IE7+, Firefox, Chrome, Opera, Safari
		  ajaxdel=new XMLHttpRequest();
		  }
		else
		  { // code for IE6, IE5
		  ajaxdel=new ActiveXObject("Microsoft.XMLHTTP");
		  }
		  
		ajaxdel.onreadystatechange=function()
		  {
			if (ajaxdel.readyState==4 && ajaxdel.status==200)
				{
				buka(controller, back, 0);
				}
			else
				{
				document.getElementById("alert").innerHTML='<img src="'+ base_url +'assets/images/loading.gif"  /> &nbsp; menghapus record ...';
				}	
		  }

		ajaxdel.open("GET",base_url +"mgpanel/"+ controller + "/" + funct +"?id="+id, true);
		ajaxdel.send();
		
		
	  }
	}		
	
	
	// notifikasi dan chat function
	
	function openchat(link)
	{
		mywind=window.open(base_url + 'mgpanel/' + link,"_blank","toolbar=0,location=0,scrollbars=1,addresbar=0,menubar=0,copyhistory=0,directories=0,status=0,resizable=yes,width=700,height=600"); mywind.moveTo(200,100);
		}
		
	
	var listen_notif = self.setInterval(
	function () 
	{
		$('#notif').load(base_url +'mgpanel/home/listen_notif');
		}, 20000);
		
	var listen_inbox = self.setInterval(
	function () 
	{
		$('#inbox').load(base_url +'mgpanel/home/listen_inbox');
		}, 20000);	
		
		
function playsound_notif()
	{
       var audio = document.getElementById("playsound_notif");
       audio.play();
    }
	
	function offNotif(controller, funct, id)
	{
	load_data = {'id':id};
	$.post(base_url +'mgpanel/notifikasi/offnotif/', load_data,  function(data) {
		$('.nothing').html(data);
	 });
	buka(controller, funct, 0);
	}
	
	function refreshnotif(){
	load_data = {'id_to':1};
	$.post(base_url +'mgpanel/notifikasi/refreshnotif/', load_data,  function(data) {
		$('#notif').html(data);
	 });
	
	 }
	
	
	//ngecek apakah ada chat baru
	window.setInterval(function(){
	load_data = {'id_to':1};
	 $.post(base_url +'mgpanel/notifikasi/cek_notif/', load_data,  function(data) {
		if(data == 'ada')
			{
			refreshnotif();				
			playsound_notif();
			}
		else
			{
			refreshnotif();
			}
	 });
	}, 7000);
	
	/* var listen_notif = self.setInterval(
	function () 
	{
		$('#notif').load(base_url +'mgpanel/notifikasi/listen_notif');
		}, 10000); */
		
	var listen_inbox = self.setInterval(
	function () 
	{
		$('#inbox').load(base_url +'mgpanel/notifikasi/listen_inbox');
		}, 30000);
		
	var listen_chat = self.setInterval(
	function () 
	{
		$('#chat').load(base_url +'mgpanel/chat/listen_chat');
		}, 7000);	
		
		
				