 // onclick hrefs
function goo(url)
	{
	window.location.href=url;
	}
	
// tombol back	
function back()
	{
	window.history.back(-1);
	}
	
function goForward() 
{
    window.history.forward()
}
	

function confirmDelete(delUrl) 
	{
		if (confirm("Apakah anda yakin ?")) 
		{
		document.location = delUrl;
		}
	}
	
function refresh() {
    location.reload();
}	
	

	
function chained(controller,id,target) 
	{
	document.getElementById(target).innerHTML='';
	$.post(base_url + controller+"/"+target, {cari: ""+id+""}, function(data)
	{$('#'+ target).html(data);});
	}
	
function add_div(controller,funct,id,target) 
	{
	document.getElementById(target).innerHTML='';
	$.post(base_url + controller+"/"+ funct +"/"+ target, {cari: ""+id+""}, function(data)
	{$('#'+ target).html(data);});
	}
	
function harusangka(jumlah)
	{
		var karakter = (jumlah.which) ? jumlah.which : event.keyCode
		if (karakter > 31 && (karakter < 48 || karakter > 57))
			{
			return false;
			}
		else
			{
			return true;
			}
	}

function ajaxpost(action,form,redirect,ubah)
	{
         $.ajax({
           url : action, 
           type: "post", //form method
           data: $("#"+ form).serialize(),
           dataType:"json", //misal kita ingin format datanya brupa json
           beforeSend:function(){
             //lakukan apasaja sambil menunggu proses selesai disini
             //misal tampilkan loading
            $("#loading").show();
			var body = $("html, body");
			body.stop().animate({scrollTop:0}, '500', 'swing');	
           },
           success:function(result){
             if(result.status){
               loadpage(redirect,ubah);
             }else{
				$("#notif_title").html('Validasi error'); 
				$("#notif_desc").html('Semua field harus diisi <br><button onclick="delikke(\'notifikasi\')" class="btn btn-primary btn-xs">Close</button>'); 
				$("#notifikasi").fadeIn(500); 
				$("#loading").hide();
             }
           },
           error: function(xhr, Status, err) {
             $("#notif_title").html('An error occured'); 
				$("#notif_desc").html(Status +'<br><button onclick="delikke(\'notifikasi\')" class="btn btn-primary btn-xs">Close</button>'); 
				$("#notifikasi").fadeIn(500); 
			 $("#loading").hide();
           }
         });
    }    	

			$('a').click(function(e) {
				ajax = $(this).attr("ajax");
				if(ajax == 'off') {}
				else
				{
				href = $(this).attr("href");
				
				loadpage(href,'ubah');
				
				// HISTORY.PUSHSTATE
				history.pushState('', '', href);
				e.preventDefault();
				
				}
			});
			
function pencarian(tujuan,formnya,ubahnya)
	{
         $.ajax({
           url : tujuan, 
           type: "post", //form method
           data: $("#"+ formnya).serialize(),
           dataType:"json", //misal kita ingin format datanya brupa json
           beforeSend:function(){
             //lakukan apasaja sambil menunggu proses selesai disini
             //misal tampilkan loading
            $("#loading2").show();
			var body = $("html, body");
			body.stop().animate({scrollTop:0}, '500', 'swing');	
           },
           success:function(result){
			$("#"+ ubahnya).html(result.result); 
			$("#loading2").hide();
           },
           error: function(xhr, Status, err) {
             $("#notif_title").html('An error occured'); 
				$("#notif_desc").html(err +'<br><button onclick="delikke(\'notifikasi\')" class="btn btn-primary btn-xs">Close</button>'); 
				$("#notifikasi").fadeIn(500); 
			 $("#loading2").hide();
           }
         });
    }
			
			window.onpopstate = function(event) {
				$("#loading").show();
				loadpage(location.pathname, 'ubah');
			};

function postform(action,form,redirect,ubah)
	{
         $.ajax({
           url : action, 
           type: "post", //form method
           data: $("#"+ form).serialize(),
           dataType:"json", //misal kita ingin format datanya brupa json
           beforeSend:function(){
             //lakukan apasaja sambil menunggu proses selesai disini
             //misal tampilkan loading
            $("#loading").show();
			var body = $("html, body");
			body.stop().animate({scrollTop:0}, '500', 'swing');	
           },
           success:function(result){
             if(result.status){
               loadpage(redirect,ubah);
			   $("#notif_title").html('Success !'); 
				$("#notif_desc").html('Data successfuly submited'); 
				$("#notifikasi").fadeIn(500); 
				$("#notifikasi").fadeOut(500); 
				$("#loading").hide();
				
             }else{
				$("#notif_title").html('Validasi error'); 
				$("#notif_desc").html(result.err +'<br><button onclick="delikke(\'notifikasi\')" class="btn btn-primary btn-xs">Close</button>'); 
				$("#notifikasi").fadeIn(500); 
				$("#loading").hide();
             }
           },
           error: function(xhr, Status, err) {
             $("#notif_title").html('An error occured'); 
				$("#notif_desc").html(err +'<br><button onclick="delikke(\'notifikasi\')" class="btn btn-primary btn-xs">Close</button>'); 
				$("#notifikasi").fadeIn(500); 
			 $("#loading").hide();
           }
         });
    }
	


function deletetbl(uri,id) 
	{
	 $.ajax({
           url : uri, 
           type: "post", 
           data: {code: id},
           dataType:"json", 
           beforeSend:function(){
            $("#loading").show();
           },
           success:function(result){
             if(result.status){
               
			   $("#tr"+ id ).remove();
				$("#notif_title").html('Success'); 
				$("#notif_desc").html('Data sucessfuly deleted '); 
				$("#notifikasi").fadeIn(500); 
				$("#notifikasi").fadeOut(1500); 
		
				$("#loading").hide();
             }else{
				$("#notif_title").html('Error'); 
			$("#notif_desc").html('Data not deleted <br> <button onclick="delikke(\'notifikasi\')" class="btn btn-info btn-xs">close</button>'); 
			$("#notifikasi").fadeIn(500); 
			$("#loading").hide();
             }
           },
           error: function(xhr, Status, err) {
             $("#notif_title").html('An error occured'); 
				$("#notif_desc").html(Status +'<br><button onclick="delikke(\'notifikasi\')" class="btn btn-primary btn-xs">Close</button>'); 
				$("#notifikasi").fadeIn(500); 
			 $("#loading").hide();
           }
         });
	}
	
function loading()
	{
	$("#loading").show();
	}	
		
function loadpage(uri,ubah) 
	{
	var body = $("html, body");
	body.stop().animate({scrollTop:0}, '500', 'swing');
	$("#loading").show();
	$.ajax(
		{
		type: "post",
		url: uri, 
		data: {code: "aragorn_ellesar"},
		success: function(result)
			{
			$("#"+ ubah).html(result);
			$("#loading").hide();
			},
		error: function (xhr, ajaxOptions, thrownError) 
			{
			$("#notif_title").html('Error'); 
			$("#notif_desc").html('Connection timeout, page not loaded <br> <button onclick="goo(\''+ uri +'\')" class="btn btn-xs btn-warning">Reload</button> &nbsp; <button onclick="delikke(\'notifikasi\')" class="btn btn-info btn-xs">Back</button>'); 
			$("#notifikasi").fadeIn(500); 
			$("#loading").hide();
			}	
		}
	);
	}
		
function loadpage_2(uri,ubah) 
	{
	var body = $("html, body");
	body.stop().animate({scrollTop:0}, '500', 'swing');
	$("#loading2").show();
	$.ajax(
		{
		type: "post",
		url: uri, 
		data: {code: "aragorn_ellesar"},
		success: function(result)
			{
			$("#"+ ubah).html(result);
			$("#loading2").hide();
			},
		error: function (xhr, ajaxOptions, thrownError) 
			{
			$("#notif_title").html('Error'); 
			$("#notif_desc").html('Connection timeout, page not loaded <br> <button onclick="goo(\''+ uri +'\')" class="btn btn-xs btn-warning">Reload</button> &nbsp; <button onclick="delikke(\'notifikasi\')" class="btn btn-info btn-xs">Back</button>'); 
			$("#notifikasi").fadeIn(500); 
			$("#loading2").hide();
			}	
		}
	);
	}
			
function delikke(id)
	{
	$("#"+id).slideUp(500);	
	}
	
function delokke(id)
	{
	$("#"+id).slideDown(500);	
	}
	
function delid(idnya)
	{
	$("#"+ idnya ).remove();
	}	
	
function fokus_ke(div_id)
	{
	$('html, body').animate({ scrollTop: $('#'+ div_id).offset().top }, 'slow');
	}	