<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * modeling untuk db function global
 * @author erwin budiantoro
 * @package models/global_model
 */
class Global_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
    
   
    function userinfo()
    {
        $sql = $this->db->query("SELECT * FROM fw_info where id_user ='1'");
        if ($sql->num_rows() > 0) {
            if ($sql !== null) {
                $this->db->close();
            }

            return $sql->row();
        } else {
            return null;
        }
    }

    function get_id_user_by_admin_id($admin_id, $table)
    {
        $sql = $this->db->query("SELECT id FROM ". $table ." where id_admin ='". $admin_id ."'");
        if ($sql->num_rows() > 0) {
            if ($sql !== null) {
                $this->db->close();
            }
            return $sql->row()->id;
        } else {
            return null;
        }
    }

    function parent_menu($id, $id_level)
    {
        $sql = $this->db->query("
	SELECT 
		fw_menu.*, 
		fw_module.controller,
		fw_access_menu.id as access
	FROM 
		fw_menu 
	left join 
		fw_module on fw_module.id= fw_menu.id_module 
	left join
		fw_access_menu on fw_access_menu.id_menu=fw_menu.id
	where 
		fw_menu.id_menu ='". $id ."' 
	and 
		fw_access_menu.id_level = '". $id_level ."'	
	order by 
		fw_menu.urutan 
	asc");
        return $sql->result();
    }

    function konversi_harga($from, $to, $value)
    {
        $sql = $this->db->query("select nilai from currency where dari ='". $from ."' and ke = '". $to ."'");
        $hasil = $sql->row()->nilai;
        $return = $hasil * $value;
    
        return $return;
    }
}
