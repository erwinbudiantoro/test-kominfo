<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * modeling untuk db function global
 * @author erwin budiantoro
 * @package models/global_model
 */
class Main_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    function insert_batch($table, $insert)
      {
      $this->db->insert_batch($table, $insert);
    	if($this->db->insert_id() > 0)
    		{
    		$return['status'] = TRUE;
    		$return['data'] = $this->db->insert_id();
    		}
    	else
    		{
    		$return['status'] = FALSE;
    		$return['data'] = $this->db->_error_message();
    		}
		return $return;
      }

    function input($table, $insert)
      {
      $this->db->insert($table, $insert);
    	if($this->db->insert_id() > 0)
    		{
    		$return['status'] = TRUE;
    		$return['data'] = $this->db->insert_id();
    		}
    	else
    		{
    		$return['status'] = FALSE;
    		$return['data'] = $this->db->_error_message();
    		}
		return $return;
      }

  	function update($table, $update, $id)
      {
  	  $this->db->where('id', $id);
      $update_data = $this->db->update($table, $update);
    	if($update_data == TRUE)
    		{
    		$return['status'] = TRUE;
    		$return['data'] = $this->db->affected_rows();
    		}
    	else
    		{
    		$return['status'] = FALSE;
    		$return['data'] = $this->db->_error_message();
    		}
  	  return $return;
      }

  	function update_where($table, $update, $col, $id)
      {
  	  $this->db->where($col, $id);
      $update_data = $this->db->update($table, $update);
    	if($update_data == TRUE)
    		{
    		$return['status'] = TRUE;
    		$return['data'] = $this->db->affected_rows();
    		}
    	else
    		{
    		$return['status'] = FALSE;
    		$return['data'] = $this->db->_error_message();
    		}
  	  return $return;
      }

  	function delete($table, $id)
      {
      $update_data = $this->db->delete($table, array('id' => $id));
    	if($update_data == TRUE)
    		{
    		$return['status'] = TRUE;
    		$return['data'] = $this->db->affected_rows();
    		}
    	else
    		{
    		$return['status'] = FALSE;
    		$return['data'] = $this->db->_error_message();
    		}
  	  return $return;
      }

  	function delete_where($table, $col, $id)
      {
      $update_data = $this->db->delete($table, array($col => $id));
    	if($update_data == TRUE)
    		{
    		$return['status'] = TRUE;
    		$return['data'] = $this->db->affected_rows();
    		}
    	else
    		{
    		$return['status'] = FALSE;
    		$return['data'] = $this->db->_error_message();
    		}
  	  return $return;
      }

  	function select($table, $id, $row=1)
      {
      $query = $this->db->query("
  		SELECT
  			*
  		FROM
  			". $table ."
  		WHERE
  			id = '". $id ."'
  		");
      if($row == 1)
        {
        return $query->row();
        }
      else
        {
        return $query->result();
        }
      }

  	function selectall($table)
      {
      $query = $this->db->query("
  		SELECT
  			*
  		FROM
  			". $table ."
  		");
      return $query->result();
      }

  	function selectwhere($table, $col, $id)
      {
      $query = $this->db->query("
  		SELECT
  			*
  		FROM
  			". $table ."
		WHERE ". $col ." = '". $id ."'
  		");
      return $query->result();
      }

}
