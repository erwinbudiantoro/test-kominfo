<?php

if(!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * class Login_model
 * modeling untuk tabel member 
 * @package models/admin
 */
class Login_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
	
    function ceklogin($username, $password)
    {
       $query = $this->db->query("SELECT * FROM users where username='". $username ."' and password='". $password ."' and active='1' ");
        if($query->num_rows() > 0)
        { 
            if($query !== NULL)
            { 
                $this->db->close();
            } 
            return $query->row();
        }
        else
        { 
            return NULL;
        }   
    }
	 
	
    function cek_token_access($token, $expired_at)
    {
	return $this->db->query("SELECT token FROM token_users WHERE  expired_at > '". $expired_at ."' AND token='". $token ."'")->row(); 
    }
	
    function cek_token($id, $expired_at)
    {
	return $this->db->query("SELECT token FROM token_users WHERE  expired_at > '". $expired_at ."' AND user_id='". $id ."'")->row(); 
    }
   
    function update_token($id, $token, $expired_at)
    {
	$this->db->query("UPDATE token_users SET token='". $token ."', expired_at = '". $expired_at ."', updated_at = '". date('Y-m-d H:i:s') ."' where user_id='". $id ."'"); 
    }
	
	 
	
}
