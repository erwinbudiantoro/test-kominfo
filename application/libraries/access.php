<?php

/**
 * class Access
 * class for checking session utk login
 * @author Erwin budiantoro
 */
class Access extends CI_Controller
{

    var $ci;
 
	function __construct() 
	{
		$this->ci = &get_instance();
		$this->ci->load->model(array('access_model'));
	}
 
	function check_session() 
	{
		if ($this->ci->session->userdata('logged_in') == TRUE)
			{
				redirect(base_url());
			}
		else
			{
			return null;
			}
	} 
	 
	function check_previleges($module) 
	{
	if ($this->ci->session->userdata('logged_in') == FALSE)
		{ 
		redirect(base_url('login'));
		} 
	}
 
}