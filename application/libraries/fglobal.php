<?php
/**
 * class Function Global
 * class utk function yang sering digunakan
 * @author erwin budiantoro
 */
class Fglobal extends CI_Controller
{
    var $ci;
 
    function __construct()
    {
        $this->ci = &get_instance();
    }
            
    function get_global_function()
    {
        $CI=&get_instance();
        $CI->load->model('global_model');
        $id_level = $this->ci->session->userdata('level');
    
        $return['fw_menu'] = $this->get_menu(0, $id_level);
        
        return $return;
    }
    
    function get_menu($id = 0, $id_level = 0)
    {
        $cntrl = $this->ci->uri->segment(1);
        $parent_menu = $this->ci->global_model->parent_menu($id, $id_level);
        $return = '';
        foreach ($parent_menu as $menu) {
            $asparent = $this->ci->global_model->parent_menu($menu->id, $id_level);
            if ($asparent == null) {
                $ajax_off = '';
            } else {
                $ajax_off = 'ajax="off"';
            }
                
            $return .= '<li>
				<a href="'. base_url() . $menu->controller .'/'. $menu->sub_module .'" '. $ajax_off .' class="';
            if ($cntrl == $menu->controller) {
                $return .=  'active';
            }
                $return .='">
					<i class="fa fa-'. $menu->fa .'"></i>
					<span>'. $menu->title .'</span>
				</a>';
                
                $return .= $this->cek_sub_menu($menu->id, '', $id_level);
                
            $return .='</li>';
        }
        return $return;
    }
        
    function cek_sub_menu($id, $strip = '', $id_level = 0)
    {
        $strip .=' &nbsp; &nbsp; ';
        $parent_menu = $this->ci->global_model->parent_menu($id, $id_level);
        $return = '';
        if ($parent_menu != null) {
            $return .= '<ul class="sub">';
            foreach ($parent_menu as $menu) {
                $asparent = $this->ci->global_model->parent_menu($menu->id, $id_level);
                if ($asparent == null) {
                    $ajax_off = '';
                } else {
                    $ajax_off = 'ajax="off"';
                }
                
                $return .= '<li><a '. $ajax_off .' href="'. base_url() . $menu->controller .'/'. $menu->sub_module .'">'. $strip .' '. $menu->title .'</a>';
                
                $return .= $this->cek_sub_menu($menu->id, $strip, $id_level);
                
                $return .='</li>';
            }
            $return .= '</ul>';
        }
        return $return;
    }
                   
    function get_id_user_by_admin_id($admin_id, $table)
    {
        $CI=&get_instance();
        $CI->load->model('global_model');
        $userinfo = $CI->global_model->get_id_user_by_admin_id($admin_id, $table);
        return $userinfo;
    }
    
    function userinfo()
    {
        $CI=&get_instance();
        $CI->load->model('global_model');
        $userinfo = $CI->global_model->userinfo();
        return $userinfo;
    }
    
    function konversi_harga($from, $to, $value)
    {
        $CI=&get_instance();
        $CI->load->model('global_model');
        $userinfo = $CI->global_model->konversi_harga($from, $to, $value);
        return $userinfo;
    }
}
