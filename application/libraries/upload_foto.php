<?php

/**
 * class Upload_Foto
 * class for upload,rezize make thumb foto 
 * @author Jogjashop
 */
class Upload_Foto extends CI_Controller
{

    var $ci;
 
	function __construct() 
	{
		$this->ci = &get_instance();
	}
	
	function start_upload($album,$fname,$pic,$thumb) 
	{
		$config['upload_path']		= $_SERVER['DOCUMENT_ROOT'].'/uploaded/images/'.$album.'/ori/';
		$config['allowed_types']	= 'gif|jpg|png|doc|pdf|ppt';
		$config['max_size']			= '1000';
		$config['max_width']		= '1024';
        $config['file_name']		= $fname;
		$this->ci->load->library('upload', $config);

		if ( ! $this->ci->upload->do_upload()) 
		{
			echo 'gagal upload';
		}
		else 
		{
			/* apabila file yang diupload terlalu besar, kita resize ke ukuran yang diinginkan */
			$conf['image_library']	= 'gd2';
			$conf['source_image']	= $_SERVER['DOCUMENT_ROOT'].'/uploaded/images/'.$album.'/ori/'.$fname;
			$conf['new_image']		= $_SERVER['DOCUMENT_ROOT'].'/uploaded/images/'.$album.'/pic/'.$fname;
			$conf['create_thumb']	= TRUE;
			$conf['maintain_ratio'] = TRUE;
			$conf['quality']		= '100%';
			$conf['width']			= $pic;
            
			chmod($conf['source_image'], 0777) ;

			$this->ci->load->library('image_lib', $conf);
			$this->ci->image_lib->initialize($conf);
			$this->ci->image_lib->resize();

			/* setelah diresize kita buat thumbnailnya */
			$conf['image_library']	= 'gd2';
			$conf['source_image']	= $_SERVER['DOCUMENT_ROOT'].'/uploaded/images/'.$album.'/ori/'.$fname;
			$conf['new_image']		= $_SERVER['DOCUMENT_ROOT'].'/uploaded/images/'.$album.'/thumb/'.$fname;
			$conf['create_thumb']	= TRUE;
			$conf['maintain_ratio'] = TRUE;
			$conf['quality']		= '100%';
			$conf['width']			= $thumb;

			chmod($conf['source_image'], 0777) ;

			$this->ci->load->library('image_lib', $conf);
			$this->ci->image_lib->initialize($conf);
			$this->ci->image_lib->resize();
			
            $this->ci->image_lib->clear() ;
		 }
	}
 
}