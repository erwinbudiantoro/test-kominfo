<?php

/**
 * class JQuery_loader
 * class for loading liraries jQuery
 * via ob_start() so you can feel speeddy :D
 * @author Angga Bayu Sejati
 */
class JQuery_loader
{

    public function __construct()
    {
        ob_start();
        
    }

    public function __destruct()
    {
        
    }

    public static function loadCore()
    {
        print '<script type="text/javascript" src="' . base_url() . 'themes/bundle/jquery-1.6.2.js"></script>
                <script type="text/javascript" src="' . base_url() . 'themes/bundle/ui/ui.core.js"></script>';
    }

    public static function loadThemes()
    {
        print '<link type="text/css" href="' . base_url() . 'themes/bundle/themes/base/ui.all.css" rel="stylesheet" />';
    }

    public static function loadDatepicker()
    {

        print "<script type='text/javascript' src='" . base_url() . "themes/bundle/ui/i18n/jquery.ui.datepicker-id.js'></script>
            <script type='text/javascript' src='" . base_url() . "themes/bundle/ui/ui.datepicker.js'></script>";
    }

    public static function formDatepicker()
    {
        print "<script>
            $(function() {
                $('#tgl').datepicker({
                    dateFormat:'yy-mm-dd',
                    changeMonth: true,
                    regional:'id',
                    changeYear: true
                });					
            });
        </script>";
    }

    public static function loadValidasi()
    {
        print '<link rel="stylesheet" href="' . base_url() . 'themes/css/validationEngine.jquery.css" type="text/css"/>
                <script type="text/javascript" src="' . base_url() . 'themes/jquery/languages/jquery.validationEngine-en.js" charset="utf-8"></script>
                <script type="text/javascript" src="' . base_url() . 'themes/jquery/jquery.validationEngine.js" charset="utf-8"></script>
              <script>
		jQuery(document).ready(function(){
			// Nama ID form untuk validasi
			jQuery("#formID").validationEngine();
		});

		function checkHELLO(field, rules, i, options){
			if (field.val() != "HELLO") {
				// this allows to use i18 for the error msgs
				return options.allrules.validate2fields.alertText;
			}
		}
	</script>';
    }

    public static function loadAutocomplete()
    {
        print "
	<script type='text/javascript' src='" . base_url() . "themes/bundle/ui/jquery.ui.widget.js'></script>
	<script type='text/javascript' src='" . base_url() . "themes/bundle/ui/jquery.ui.position.js'></script>
	<script type='text/javascript' src='" . base_url() . "themes/bundle/ui/jquery.ui.autocomplete.js'></script>";
    }

    public static function autocompleteDataObat($arrayData)
    {

        print "<script >
	$(function() {
		var availableTags = ['aku', 'kamu'];
		$('#id_auto').autocomplete({
			source: availableTags
		});
	});
	</script>";
    }

    public static function loadMenu()
    {
        print "
		<script language='javascript' type='text/javascript' src='".  base_url()."themes/jquery/treemenu.js'></script>
		<script language='javascript' type='text/javascript' src='".  base_url()."themes/jquery/resizeiframe.js'></script>
		<link rel='stylesheet' type='text/css' href='".  base_url()."themes/jsmenu/ddlevelsfiles/ddlevelsmenu-base.css' />
		<link rel='stylesheet' type='text/css' href='".  base_url()."themes/jsmenu/ddlevelsfiles/ddlevelsmenu-topbar.css' />
		<link rel='stylesheet' type='text/css' href='".  base_url()."themes/jsmenu/ddlevelsfiles/ddlevelsmenu-sidebar.css' />
		<script type='text/javascript' src='".  base_url()."themes/jsmenu/ddlevelsfiles/ddlevelsmenu.js'></script>";
    }

}
