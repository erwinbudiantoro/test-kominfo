<?php

/**
 * class Ckeditor
 * class ckeditor konfiguasi ckfinder
 * via ob_start() so you can feel speeddy :D
 * @author Jogjashop
 */
class Ckeditor extends CI_Controller
{

    var $ci;
 
	function __construct() 
	{
		$this->ci = &get_instance();
	}
	
	
	//konfigurasi ckfinder dengan ckeditor  
	function text_editor() 
	{  
		$this->ci->load->library('ckeditor');  
		$this->ci->load->library('ckfinder');  
		$this->ci->ckeditor->basePath = base_url().'js/ckeditor/';  
		$this->ci->ckeditor->config['toolbar'] = 'Full';  
		$this->ci->ckeditor->config['language'] = 'en';           
		$this->ci->ckfinder->SetupCKEditor($this->ckeditor,'../../js/ckfinder/');  
	}
 
}