<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">

    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Erwin Budiantoro">
    <link rel="shortcut icon" href="<?php echo base_url('assets/images/favicon.png'); ?>">
    <title><?php echo $title; ?></title>
    <!--Core CSS -->
    <link href="<?php echo base_url('assets/bs3/css/bootstrap.min.css'); ?>" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="<?php echo base_url('assets/css/style.css'); ?>" rel="stylesheet">
</head>

  <body class="lock-screen">
    <div class="container">
      <form id="contoh-form" method="post" class="form-signin" >
        <h2 class="form-signin-heading"><?php echo $title; ?></h2>
        <div class="login-wrap">

            <div class="user-login-info">
		<div id="alert" style="display:none;" class="alert alert-warning">
			<button type="button" class="close" onclick="delikke()">&times;</button>Error ! Kata kunci tidak dikenali</div>		
                <input onKeyDown="if(event.keyCode==13) ajaxpost();" type="text" required="required" name="username" class="form-control" placeholder="Username" autofocus>
                <input onKeyDown="if(event.keyCode==13) ajaxpost();" type="password" required="required"	 name="password" class="form-control" placeholder="Kata Kunci">
            </div>
            <div id="login"><button class="btn btn-lg btn-login btn-block" onclick="ajaxpost()" type="button">Masuk</button></div>
        </div>
      </form>
    </div>
    <!--Core js-->
    <script src="<?php echo base_url('assets/js/jquery.js'); ?>"></script>
    <script src="<?php echo base_url('assets/bs3/js/bootstrap.min.js'); ?>"></script>
    <script>
	function delikke()
		{
		$("#alert").hide();
		}
	
	function ajaxpost()
	{
         $.ajax({
           url : '<?php echo base_url('login/do_login');?>', 
           type: "post",
           data: $("#contoh-form").serialize(),
           dataType:"json", 
           beforeSend:function(){
             
            document.getElementById("login").innerHTML='<center><img height="30" src="<?php echo base_url('assets/images/loading.gif');?>"></center>';
			
           },
           success:function(result){
             
             if(result.status){
                document.getElementById("login").innerHTML='<center>Redirect..</center>';
               window.location.href='<?php echo base_url('home');?>';

             }else{
               $("#alert").show();
			   document.getElementById("login").innerHTML='<button class="btn btn-lg btn-login btn-block" onclick="ajaxpost()" type="button">Masuk</button>';
             }
             
           },
           error: function(xhr, Status, err) {
             
             alert("Terjadi error, harap ulangi : "+Status);
			  document.getElementById("login").innerHTML='<button class="btn btn-lg btn-login btn-block" onclick="ajaxpost()" type="button">Masuk</button>';
           }
           
         });
    }       
	</script>
  </body>
</html>
