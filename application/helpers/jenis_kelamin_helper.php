<?php  if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

if (! function_exists('jenis_kelamin')) {
    function jenis_kelamin($kelamin)
    {
        $jenisKelamin = "";
        if (strtolower($kelamin) == "l") {
            $jenisKelamin = "Laki - Laki";
        }
        if (strtolower($kelamin) == "p") {
            $jenisKelamin = "Perempuan";
        }

        return $jenisKelamin;
    }
}
