<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('tgl_indo_dmy'))
{
	function tgl_indo_dmy($tanggal)
	{
		$xplode = explode('-', $tanggal);
		$bln = $xplode[1];
		$tgl = $xplode[2];
		$thn = $xplode[0];
		$tgl_mdy = $thn .'-'. $bln .'-'. $tgl;
		
		$ubah = gmdate($tgl_mdy, time()+60*60*8);
		$pecah = explode("-",$ubah);
		$tanggal = substr($pecah[2],0,2);
		$bulan = bulan($pecah[1]);
		$tahun = $pecah[0];
		return $tanggal.' '.$bulan.' '.$tahun;
	}
}

if ( ! function_exists('tgl_indo'))
{
	function tgl_indo($tgl=null)
	{
	if($tgl == null)
		{
		return '<font colo="red"><small>Date not set</small></font>';
		}
	elseif($tgl == '0000-00-00')
		{
		return '<font colo="red"><small>Date not set</small></font>';
		}
	else
		{
		$xplode = explode('-', $tgl);
		$bln = $xplode[1];
		$tgl = $xplode[2];
		$thn = $xplode[0];
		$tgl_mdy = $thn .'-'. $bln .'-'. $tgl;
		
		$ubah = gmdate($tgl_mdy, time()+60*60*8);
		$pecah = explode("-",$ubah);
		$tanggal = substr($pecah[2],0,2);
		$bulan = bulan($pecah[1]);
		$tahun = $pecah[0];
		return $tanggal.' '.$bulan.' '.$tahun;
		}
	}
}

if ( ! function_exists('bulan'))
{
	function bulan($bln)
	{
		switch ($bln)
		{
			case 1:
				return "Januari";
				break;
			case 2:
				return "Februari";
				break;
			case 3:
				return "Maret";
				break;
			case 4:
				return "April";
				break;
			case 5:
				return "Mei";
				break;
			case 6:
				return "Juni";
				break;
			case 7:
				return "Juli";
				break;
			case 8:
				return "Agustus";
				break;
			case 9:
				return "September";
				break;
			case 10:
				return "Oktober";
				break;
			case 11:
				return "November";
				break;
			case 12:
				return "Desember";
				break;
		}
	}
}

if ( ! function_exists('nama_hari'))
{
	function nama_hari($tanggal)
	{
		$ubah = gmdate($tanggal, time()+60*60*8);
		$pecah = explode("-",$ubah);
		$tgl = $pecah[2];
		$bln = $pecah[1];
		$thn = $pecah[0];

		$nama = date("l", mktime(0,0,0,$bln,$tgl,$thn));
		$nama_hari = "";
		if($nama=="Sunday") {$nama_hari="Minggu";}
		else if($nama=="Monday") {$nama_hari="Senin";}
		else if($nama=="Tuesday") {$nama_hari="Selasa";}
		else if($nama=="Wednesday") {$nama_hari="Rabu";}
		else if($nama=="Thursday") {$nama_hari="Kamis";}
		else if($nama=="Friday") {$nama_hari="Jumat";}
		else if($nama=="Saturday") {$nama_hari="Sabtu";}
		return $nama_hari;
	}
}

if ( ! function_exists('hitung_mundur'))
{
	function hitung_mundur($wkt)
	{
		$waktu=array(	365*24*60*60	=> "tahun",
						30*24*60*60		=> "bulan",
						7*24*60*60		=> "minggu",
						24*60*60		=> "hari",
						60*60			=> "jam",
						60				=> "menit",
						1				=> "detik");

		$hitung = strtotime(gmdate ("Y-m-d H:i:s", time () +60 * 60 * 8))-$wkt;
		$hasil = array();
		if($hitung<5)
		{
			$hasil = 'kurang dari 5 detik yang lalu';
		}
		else
		{
			$stop = 0;
			foreach($waktu as $periode => $satuan)
			{
				if($stop>=6 || ($stop>0 && $periode<60)) break;
				$bagi = floor($hitung/$periode);
				if($bagi > 0)
				{
					$hasil[] = $bagi.' '.$satuan;
					$hitung -= $bagi*$periode;
					$stop++;
				}
				else if($stop>0) $stop++;
			}
			$hasil=implode(' ',$hasil).' yang lalu';
		}
		return $hasil;
	}
}

if ( ! function_exists('jam'))
{
	function jam($wkt)
	{
	$hasil = substr($wkt, 11, 8);
	return $hasil;
	}
}


if ( ! function_exists('tgl_to_int'))
{
	function tgl_to_int($tgl)
	{
	if(substr($tgl,0,1) == '0')
		{
		return substr($tgl,1,1);
		}
	else
		{
		return $tgl;
		}
	}
}

if ( ! function_exists('int_to_tgl'))
{
	function int_to_tgl($int)
	{
	if(strlen($int) == 1)
		{
		return  '0'. $int;
		}
	else
		{
		return  $int;
		}
	}
}



// fungsi untuk mencari selisih antara 2 tanggal
// return nya adalah hari, jam, menit, detik
// format tnggal dan jam aalah : gmdate("Y-m-d H:i:s", time()+60*60*7); atau brgini : '2014-06-10 21:50:41';
if ( ! function_exists('selisih_jam_tanggal'))
{
	function selisih_jam_tanggal($start, $finish)
	{
	
	$awal = $start;

	$akhir = $finish;
	
	$jam_awal = substr($awal, 11, 2);
	$menit_awal = substr($awal, 14, 2);
	$detik_awal = substr($awal, 17, 2);
	$sisa_detik_awal = $detik_awal + (($menit_awal * 60) + ($jam_awal * 3600));

	$jam_akhir = substr($akhir, 11, 2);
	$menit_akhir = substr($akhir, 14, 2);
	$detik_akhir = substr($akhir, 17, 2);
	$sisa_detik_akhir = $detik_akhir + (($menit_akhir * 60) + ($jam_akhir * 3600));

	if($sisa_detik_akhir > $sisa_detik_awal)
		{
		$selisihwaktu = $sisa_detik_akhir - $sisa_detik_awal;
		}
	else
		{
		$detik24jam = 86400;
		$selisihwaktu = $sisa_detik_akhir + ($detik24jam - $sisa_detik_awal);
		}
		
	//konvert detik ke jam menit detik
	$data['sisa_jam'] = floor($selisihwaktu/3600);

	//Untuk menghitung jumlah dalam satuan menit:
	$sisa = $selisihwaktu % 3600;
	$data['sisa_menit'] = floor($sisa/60);

	//Untuk menghitung jumlah dalam satuan detik:
	$sisa = $sisa % 60;
	$data['sisa_detik'] = floor($sisa/1);

	$tgl1 = substr($awal,0,10); 
	$tgl2 = substr($akhir,0,10);

	// memecah tanggal untuk mendapatkan bagian tanggal, bulan dan tahun
	// dari tanggal pertama

	$pecah1 = explode("-", $tgl1);
	$date1 = $pecah1[2];
	$month1 = $pecah1[1];
	$year1 = $pecah1[0];

	// memecah tanggal untuk mendapatkan bagian tanggal, bulan dan tahun
	// dari tanggal kedua

	$pecah2 = explode("-", $tgl2); 
	$date2 = $pecah2[2];
	$month2 = $pecah2[1];
	$year2 =  $pecah2[0];

	// menghitung JDN dari masing-masing tanggal
	$jd1 = gregoriantojd($month1, $date1, $year1);
	$jd2 = gregoriantojd($month2, $date2, $year2);

	// hitung selisih hari kedua tanggal

	$data['selisih_hari'] = $jd2 - $jd1;
	
	$sisa_umur_dlm_hari = $data['selisih_hari'] % 365;
	$data['umur'] = floor($data['selisih_hari'] / 365) .' Tahun, '. floor($sisa_umur_dlm_hari / 30) .' Bulan';
	
	$selisihdetik = $selisihwaktu + ($data['selisih_hari'] * 86400);
	$data['selisihdetik'] = $selisihdetik;

	if($selisihdetik < 60)
		{
		 $data['yang_lalu'] = 'Just now';
		}
	elseif(($selisihdetik > 60) and ($selisihdetik < 3600) )
		{
		$menit = floor($selisihdetik / 60);
		 $data['yang_lalu'] =  $menit .' minutes ago';
		}
	elseif(($selisihdetik > 3600) and ($selisihdetik < 86400))
		{
		$jam = floor($selisihdetik / 3600);
		 $data['yang_lalu'] =  $jam .' hours ago';
		}
	elseif(($selisihdetik > 86400) and ($selisihdetik < 2592000))
		{
		$hari = floor($selisihdetik / 86400);
		 $data['yang_lalu'] =  $hari .' days ago';
		}
	else
		{
		$bulan = floor($selisihdetik / 2592000);
		 $data['yang_lalu'] = $bulan .' month ago';
		}



	return $data; 
	}
}