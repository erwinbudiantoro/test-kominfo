<?php

// untuk debugging
if (!function_exists('alive')) {

    function alive($data) {
        echo '<pre class="-debug">';
        print_r($data);
        echo '</pre>' . "\n";
    }

}

if (!function_exists('dead')) {

    function dead($data) {
        echo '<pre class="-debug">';
        print_r($data);
        echo '</pre>' . "\n";
		die();
    }

}

if (!function_exists('clean')) {

    function clean($data) {
    $new =  str_replace("'","", $data);
    return $new;
	}

}

if (!function_exists('between')) {

   function between($val, $min, $max)
   {
   if($val >= $min && $val <= $max) 
	{ return true; }
   else
	{ return false; }
   }

}

if (!function_exists('format_rupiah')) {

   function format_rupiah($angka)
	{
	$rupiah =  number_format($angka,0,',','.');
	return $rupiah .'';
	}

}

if (!function_exists('nmr')) {

      function nmr($angka)
	{
	$rupiah =  number_format($angka,0,',','.');
	return $rupiah;
	}

}
if (!function_exists('nomor')) {

      function nomor($angka)
	{
	$rupiah =  number_format($angka,0,',','.');
	return $rupiah;
	}

}

if (!function_exists('ifnull')) {

    function ifnull($angka)
	{
	if($angka == null){
		return 0;
	}else{
		return $angka;
	}
	}

}
  
if (!function_exists('diskon')) {

   function diskon($total, $diskon)
	{
	$proses1 = $total / 100;
	$proses2 = $proses1 * $diskon;
	$data['jml_diskon'] = $proses2;
	$data['hasil'] = $total - $proses2;
	
	return $data;
	}

}

if (!function_exists('tanggal_to_ymd')) {

   function tanggal_to_ymd($tanggal)
	{	
	$explode = explode('-', $tanggal);
	$return = $explode[2] .'-'. $explode[1] .'-'. $explode[0];
	return $return;
	}

}

if (!function_exists('rupiah_to_num')) {

   function rupiah_to_num($rupiah)
	{	
	$remove1 = str_replace('', '', $rupiah);
	$remove2 = str_replace('.', '', $remove1);
	$gantititik = str_replace(',', '.', $remove2);
	
	$return = $gantititik;
	return $return;
	}

}

if (!function_exists('yesno')) {

   function yesno($yesno)
	{	
	if($yesno == 1)
		{
		return 'Yes';	
		}
	else
		{
		return 'No';	
		}	
	}

}


if (!function_exists('url_origin')) {
    function url_origin($s, $use_forwarded_host=false)
	{
    $ssl = (!empty($s['HTTPS']) && $s['HTTPS'] == 'on') ? true:false;
    $sp = strtolower($s['SERVER_PROTOCOL']);
    $protocol = substr($sp, 0, strpos($sp, '/')) . (($ssl) ? 's' : '');
    $port = $s['SERVER_PORT'];
    $port = ((!$ssl && $port=='80') || ($ssl && $port=='443')) ? '' : ':'.$port;
    $host = ($use_forwarded_host && isset($s['HTTP_X_FORWARDED_HOST'])) ? $s['HTTP_X_FORWARDED_HOST'] : (isset($s['HTTP_HOST']) ? $s['HTTP_HOST'] : null);
    $host = isset($host) ? $host : $s['SERVER_NAME'] . $port;
    return $protocol . '://' . $host;
	}
}

if (!function_exists('full_url')) {
    function full_url($s, $use_forwarded_host=false)
		{
			return url_origin($s, $use_forwarded_host) . $s['REQUEST_URI'];
		}
}

if (!function_exists('filtering_parameter_query')) {
	function filtering_parameter_query($list)
		{
		foreach($list as $key=>$value)
			{
			$filter[$key] = utf8_decode(urldecode(str_replace("'","\'", $value)));
			}
		return $filter;	
		}
}

if (!function_exists('filtering_parameter_view')) {
	function filtering_parameter_view($list)
		{
		foreach($list as $key=>$value)
			{
			if($value == '0')
				{
				$replace_nol = str_replace('0','', $value);
				}
			else
				{
				$replace_nol = $value;
				}
			$filter[$key] = utf8_decode(urldecode(str_replace("'","\'", $replace_nol)));
			}
		return $filter;	
		}
}


if (!function_exists('col_to_string')) {
	function col_to_string($var)
		{
		$var = str_replace('id_','', $var);
		$return =  ucwords(str_replace('_', ' ', $var));
		return $return;	
		}
}

function createlink( $string, $separator = '-' )
{
    $accents_regex = '~&([a-z]{1,2})(?:acute|cedil|circ|grave|lig|orn|ring|slash|th|tilde|uml);~i';
    $special_cases = array( '&' => 'and', "'" => '');
    $string = mb_strtolower( trim( $string ), 'UTF-8' );
    $string = str_replace( array_keys($special_cases), array_values( $special_cases), $string );
    $string = preg_replace( $accents_regex, '$1', htmlentities( $string, ENT_QUOTES, 'UTF-8' ) );
    $string = preg_replace("/[^a-z0-9]/u", "$separator", $string);
    $string = preg_replace("/[$separator]+/u", "$separator", $string);
    return $string;
}
 
function cekimage($url)
{
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL,$url);
    // don't download content
    curl_setopt($ch, CURLOPT_NOBODY, 1);
    curl_setopt($ch, CURLOPT_FAILONERROR, 1);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    if(curl_exec($ch)!==FALSE)
    {
        return 1;
    }
    else
    {
        return 0;
    }
}

function harga_pembelian($var)
{	
	if(strlen($var) > 0){
	   $formula[0] = 'A';
	   $formula[1] = 'B';
	   $formula[2] = 'C';
	   $formula[3] = 'D';
	   $formula[4] = 'E';
	   $formula[5] = 'F';
	   $formula[6] = 'G';
	   $formula[7] = 'H';
	   $formula[8] = 'I';
	   $formula[9] = 'J';
	   $return = '';	
	   $split = str_split($var);
	   foreach($split as $sp){
		if(($sp > -1) && ($sp < 10)){
			$return .= $formula[$sp];
		}else{
			$return = 'N/A';
			break;
		}
	   }
	}else{
		$return = 0;	
	}

return $return; 
}

function harga_jual($var)
{	
	if(strlen($var) > 0){
	   $formula[0] = 'A';
	   $formula[1] = 'B';
	   $formula[2] = 'C';
	   $formula[3] = 'D';
	   $formula[4] = 'E';
	   $formula[5] = 'F';
	   $formula[6] = 'G';
	   $formula[7] = 'H';
	   $formula[8] = 'I';
	   $formula[9] = 'J';
	   $return = '';	
	   $split = str_split($var);
	   foreach($split as $sp){
		if(($sp > -1) && ($sp < 10)){
			$return .= $formula[$sp];
		}else{
			$return = 'N/A';
			break;
		}
	   }
	}else{
		$return = 0;	
	}

return $return; 
}

function harga_reseller($var)
{	
	if(strlen($var) > 0){
	   $formula[0] = 'A';
	   $formula[1] = 'Bz';
	   $formula[2] = 'Cs';
	   $formula[3] = 'Ds';
	   $formula[4] = 'Es';
	   $formula[5] = 'Fx';
	   $formula[6] = 'G';
	   $formula[7] = 'H';
	   $formula[8] = 'I';
	   $formula[9] = 'J';
	   $return = '';	
	   $split = str_split($var);
	   foreach($split as $sp){
		if(($sp > -1) && ($sp < 10)){
			$return .= $formula[$sp];
		}else{
			$return = 'N/A';
			break;
		}
	   }
	}else{
		$return = 0;	
	}

return $return; 
}