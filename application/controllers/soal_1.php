<?php
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Soal_1 extends CI_Controller
{

    public function __construct()
    {
        parent::__construct(); 
        $this->load->model(array('main_model'));
    }

    function index()
    { 
	 $source_data = 'http://localhost/test-kominfo/storage/';	
	 $users = $this->get_curl($source_data.'users.json');
	 $albums = $this->get_curl($source_data.'albums.json');
	 $photos = $this->get_curl($source_data.'photos.json');
	 $posts = $this->get_curl($source_data.'posts.json');
	 $todos = $this->get_curl($source_data.'todos.json');
		
	 $response = new stdClass();
	 $response->message = 'success'; 

	 $result_data = array();
	 $user_key = 0;
	 foreach($users as $usr){ 
		// get country name 
		$get_country = $this->get_country($usr->address->geo->lat, $usr->address->geo->lng); 

		// count post 
		$countPost = 0;
		foreach($posts as $var){
			if($usr->id == $var->userId){
				$countPost++;
			}
		}

		// count album 
		$countalbum = 0;
		// count photos 
		$countPhotos = 0;
		foreach($albums as $var){
			if($usr->id == $var->userId){
				$countalbum++; 
				foreach($photos as $ph){
					if($var->id == $ph->albumId){
						$countPhotos++;
					}
				}
				
			}
		}

		
		// count post 
		$counttodos = 0;
		foreach($todos as $var){
			if($usr->id == $var->userId){
				$counttodos++;
			}
		}

		$response->data[$user_key] = new stdClass();
		$response->data[$user_key]->name = $usr->name;
		$response->data[$user_key]->username = $usr->username;
		$response->data[$user_key]->country = $get_country;  
		$response->data[$user_key]->website = $usr->website;
		$response->data[$user_key]->phone = $usr->phone;
		$response->data[$user_key]->total_post = $countPost;  
		$response->data[$user_key]->total_todo = $counttodos;  
		$response->data[$user_key]->total_album = $countalbum;  
		$response->data[$user_key]->total_photo = $countPhotos; 
		$response->data[$user_key]->created_at = date('Y-m-d H:i:s'); 
		$response->data[$user_key]->updated_at = ''; 
		$response->data[$user_key]->deleted_at = ''; 
		$user_key++;
	 }

	 
	 header('Content-Type: application/json; charset=utf-8');
	 echo json_encode($response);
	}

	function get_curl($url){  
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);   
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
		curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json')); 
		$output = curl_exec($ch);
		curl_close($ch);
	
		return json_decode($output);
	}
 
	function get_country($lat,$lng){  
		 $ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, 'http://api.geonames.org/countryCodeJSON?formatted=true&lat='. $lat .'&lng='. $lng .'&username=Plausible0649&style=full');   
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
		curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json')); 
		$output = curl_exec($ch); 
		curl_close($ch);
	
		$return = json_decode($output);
		return $return->countryName; 
	}

}
