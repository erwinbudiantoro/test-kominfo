<?php
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Soal_2 extends CI_Controller
{

    public function __construct()
    {
        parent::__construct(); 
        $this->load->model(array('main_model','login_model'));
		$this->load->helper('x_helper');
        $this->load->helper('form');
        $this->load->library('form_validation');
		$this->load->library('session'); 
    }

    function login(){
		$this->form_validation->set_rules('username', 'username', 'required|xss_clean');
		$this->form_validation->set_rules('password', 'password', 'required|xss_clean');
		$this->form_validation->set_error_delimiters('', '<br/>');
		if($this->form_validation->run() == TRUE){
			$username = clean($this->input->post('username'));
			$password = md5(md5($this->input->post('password')));

			$cek_login = $this->login_model->ceklogin($username, $password);

			if($cek_login == NULL)
			{	
				header('Content-Type: application/json; charset=utf-8');
				echo json_encode(
					array(
						'message'=>'Username atau Password tidak dikenali', 
						'status'=>0,
					)
				);
			}
			else
			{ 
				//cek token 
				$now = date('Y-m-d H:i:s');
				$cek_token = $this->login_model->cek_token($cek_login->id, $now);

				// cek apakah token masih valid
				if($cek_token != NULL){
					$token = $cek_token->token;
				}else{	
					$expired_at = date("Y-m-d H:i:s", strtotime("+5 minutes"));
					$token = md5($cek_login->id .'_'. date('ymdhis')); 
         			$this->login_model->update_token($cek_login->id, $token, $expired_at);
				}
 
				header('Content-Type: application/json; charset=utf-8');
				echo json_encode(
					array(
						'message'=>'Successfully logged in',
						'token_key'=>$token,
						'status'=>1,
					)
				);
			}
		}else{
		header('Content-Type: application/json; charset=utf-8');
				echo json_encode(
					array(
						'message'=>'ILLEGAL REQUEST', 
						'status'=>0,
					)
				);
		}
	}

	function fetchdata(){
		$get_headers = getallheaders();
		$token = $get_headers['Authorization'];
		//cek token 
		$now = date('Y-m-d H:i:s');
		$cek_token = $this->login_model->cek_token_access($token, $now);

		// cek apakah token masih valid
		if($cek_token != NULL){
			header('Content-Type: application/json; charset=utf-8');
			echo json_encode(
				array(
					'message'=>'Already inserted to DB', 
					'status'=>1,
				)
			); 
		}else{	
			header('Content-Type: application/json; charset=utf-8');
			echo json_encode(
				array(
					'message'=>'Token Expired', 
					'status'=>0,
				)
			); 
		}

		
	}

	function update_data($id){
		$get_headers = getallheaders();

		// variable 
		$website = $this->input->post('website');
		$phone = $this->input->post('phone');

		$token = $get_headers['Authorization'];

		//cek token 
		$now = date('Y-m-d H:i:s');
		$cek_token = $this->login_model->cek_token_access($token, $now);

		// cek apakah token masih valid
		if($cek_token != NULL){

			$this->db->query("
				UPDATE results SET website = '". $website ."', phone = '". $phone ."' WHERE id = ". $id ."
			");

			header('Content-Type: application/json; charset=utf-8');
			echo json_encode(
				array(
					'message'=>'Data Updated', 
					'status'=>1, 
				)
			); 
		}else{	
			header('Content-Type: application/json; charset=utf-8');
			echo json_encode(
				array(
					'message'=>'Token Expired', 
					'status'=>0,
				)
			); 
		}

		
	}

	function delete_data(){
		$get_headers = getallheaders();

		// variable 
		$id = $this->input->post('id'); 

		$token = $get_headers['Authorization'];

		//cek token 
		$now = date('Y-m-d H:i:s');
		$cek_token = $this->login_model->cek_token_access($token, $now);

		// cek apakah token masih valid
		if($cek_token != NULL){

			$this->db->query("
				UPDATE results SET deleted_at = '". $now ."' WHERE id = ". $id ."
			");

			header('Content-Type: application/json; charset=utf-8');
			echo json_encode(
				array(
					'message'=>'Data Deleted', 
					'status'=>1, 
				)
			); 
		}else{	
			header('Content-Type: application/json; charset=utf-8');
			echo json_encode(
				array(
					'message'=>'Token Expired', 
					'status'=>0,
				)
			); 
		}

		
	}

	function results(){
		$get_headers = getallheaders();

		// variable 
		$limit = $this->input->post('limit');
		$page = $this->input->post('page');

		$token = $get_headers['Authorization'];
		//cek token 
		$now = date('Y-m-d H:i:s');
		$cek_token = $this->login_model->cek_token_access($token, $now);

		// cek apakah token masih valid
		if($cek_token != NULL){

			$data_result = $this->db->query("
				SELECT * FROM results WHERE deleted_at IS NULL ORDER BY id DESC LIMIT ". $page .", ". $limit ."
			")->result();

			header('Content-Type: application/json; charset=utf-8');
			echo json_encode(
				array(
					'message'=>'', 
					'status'=>1,
					'data_result'=> $data_result,
				)
			); 
		}else{	
			header('Content-Type: application/json; charset=utf-8');
			echo json_encode(
				array(
					'message'=>'Token Expired', 
					'status'=>0,
				)
			); 
		}

		
	}

	function get_details($id){
		$get_headers = getallheaders();
		$token = $get_headers['Authorization'];
		//cek token 
		$now = date('Y-m-d H:i:s');
		$cek_token = $this->login_model->cek_token_access($token, $now);

		// cek apakah token masih valid
		if($cek_token != NULL){

			$data_result = $this->db->query("
				SELECT * FROM results WHERE id = ". $id ."
			")->row();

			header('Content-Type: application/json; charset=utf-8');
			echo json_encode(
				array(
					'message'=>'', 
					'status'=>1,
					'data_result'=> $data_result,
				)
			); 
		}else{	
			header('Content-Type: application/json; charset=utf-8');
			echo json_encode(
				array(
					'message'=>'Token Expired', 
					'status'=>0,
				)
			); 
		}

		
	}

	
}