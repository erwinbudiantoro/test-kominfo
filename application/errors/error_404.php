
<!DOCTYPE html>
<html dir="ltr" lang="en">

<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<head>
<meta charset="UTF-8">
<title>Halaman Tidak Ditemukan</title>

</head>
<style type="text/css">

body {
	margin: 200px;
	text-align:center;
}
</style>
<body>
	<div align="center" id="container">
		<h1><?php echo $heading; ?></h1>
		<?php echo $message; ?>
		<a href="/">Kembali ke halaman utama</a>
	</div>
</body>
</html>