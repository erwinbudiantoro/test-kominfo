-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Waktu pembuatan: 15 Sep 2022 pada 06.57
-- Versi server: 10.4.19-MariaDB
-- Versi PHP: 7.3.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `test_kominfo`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `results`
--

CREATE TABLE `results` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `country` varchar(60) DEFAULT NULL,
  `website` varchar(100) DEFAULT NULL,
  `phone` varchar(60) DEFAULT NULL,
  `total_post` int(7) DEFAULT NULL,
  `total_todo` int(7) DEFAULT NULL,
  `total_album` int(7) DEFAULT NULL,
  `total_photo` int(7) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `results`
--

INSERT INTO `results` (`id`, `name`, `username`, `country`, `website`, `phone`, `total_post`, `total_todo`, `total_album`, `total_photo`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Leanne Graham', 'Bret', 'Brazil', 'hildegard.org', '1-770-736-8031 x56442', 9, 19, 7, 350, '2022-09-15 10:44:26', NULL, NULL),
(2, 'Ervin Howell', 'Antonette', 'Switzerland', 'anastasia.net', '010-692-6593 x09125', 8, 22, 9, 450, '2022-09-15 10:44:27', NULL, NULL),
(3, 'Clementine Bauch', 'Samantha', 'China', 'ramiro.info', '1-463-123-4447', 11, 23, 7, 350, '2022-09-15 10:44:27', NULL, NULL),
(4, 'Patricia Lebsack', 'Karianne', 'Germany', 'kale.biz', '493-170-9623 x156', 15, 20, 6, 300, '2022-09-15 10:44:28', NULL, NULL),
(5, 'Chelsey Dietrich', 'Kamren', 'Ecuador', 'demarco.info', '(254)954-1289', 9, 16, 12, 600, '2022-09-15 10:44:28', NULL, NULL),
(6, 'Mrs. Dennis Schulist', 'Leopoldo_Corkery', 'Antarctica', 'ola.org', '1-477-935-8478 x6430', 7, 20, 12, 600, '2022-09-15 10:44:29', NULL, NULL),
(7, 'Kurtis Weissnat', 'Elwyn.Skiles', 'Spain', 'elvis.io', '210.067.6132', 10, 24, 15, 750, '2022-09-15 10:44:30', NULL, NULL),
(8, 'Nicholas Runolfsdottir V', 'Maxime_Nienow', 'France', 'www.anyweb.com', '088171616112', 8, 19, 11, 550, '2022-09-15 10:44:30', NULL, '2022-09-15 11:56:48'),
(9, 'Glenna Reichert', 'Delphine', 'Greece', 'conrad.com', '(775)976-6794 x41206', 16, 20, 14, 700, '2022-09-15 10:44:31', NULL, NULL),
(10, 'Clementina DuBuque', 'Moriah.Stanton', 'Indonesia', 'ambrose.net', '024-648-3804', 7, 17, 7, 350, '2022-09-15 10:44:31', NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `token_users`
--

CREATE TABLE `token_users` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `token` varchar(50) DEFAULT NULL,
  `expired_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `token_users`
--

INSERT INTO `token_users` (`id`, `user_id`, `token`, `expired_at`, `created_at`, `updated_at`) VALUES
(1, 1, 'da5186c0fee3b50924b04e9a5a534160', '2022-09-15 11:57:11', '2022-09-15 05:50:03', '2022-09-15 11:52:11');

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `fullname` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `active` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `fullname`, `created_at`, `deleted_at`, `active`) VALUES
(1, 'erwin', '550e1bafe077ff0b0b67f4e32f29d751', 'Erwin Budiantoro', '2022-09-15 05:49:27', NULL, 1);

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `results`
--
ALTER TABLE `results`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `token_users`
--
ALTER TABLE `token_users`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `results`
--
ALTER TABLE `results`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT untuk tabel `token_users`
--
ALTER TABLE `token_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
